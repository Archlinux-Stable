#! /bin/sh
# Implement blacklisting for udev-loaded modules
#   Includes module checking
# - Aaron Griffin & Tobias Powalowski for Archlinux
. /etc/rc.conf

[ $# -ne 1 ] && exit 1

if [ -f /proc/cmdline ]; then 
	for cmd in $(cat /proc/cmdline); do
    		case $cmd in
        		*=*) eval $cmd ;;
    		esac
	done
fi

# blacklist framebuffer modules
for x in $(echo /lib/modules/$(uname -r)/kernel/drivers/video/*/*fb*); do
	BLACKLIST="$BLACKLIST $(basename $x .ko)"
done
for x in $(echo /lib/modules/$(uname -r)/kernel/drivers/video/*fb*); do
	BLACKLIST="$BLACKLIST $(basename $x .ko)"
done
# get the real names from modaliases
i="$(/sbin/modprobe -i --show-depends $1 | sed "s#^insmod /lib.*/\(.*\)\.ko.*#\1#g" | sed 's|-|_|g')"
# get blacklistet modules
k="$(echo $BLACKLIST ${MOD_BLACKLIST[@]} | sed 's|-|_|g')"
j="$(echo ${MODULES[@]} | sed 's|-|_|g')"
#add disabled MODULES (!) to blacklist - much requested feature
for m in ${j}; do
    [ "$m" != "${m#!}" ] && k="${k} ${m#!}"
done
# add disablemodules= from commandline to blacklist
k="${k} $(echo ${disablemodules} | sed 's|-|_|g' | sed 's|,| |g')"


if [ "$MOD_AUTOLOAD" = "yes" -o "$MOD_AUTOLOAD" = "YES" ] && [ "$load_modules" != "off" ]; then
	if [ "${k}" != "" ]; then 
        	for n in ${i}; do
            	if echo ${k} | /bin/grep "\<$n\>" 2>&1 >/dev/null; then
                	/usr/bin/logger -p info \
                	"udev load-modules: $i is blacklisted"
                	exit 1
            	fi
        	done
	fi
    /sbin/modprobe $1
else
    /usr/bin/logger -p info \
        "udev load-modules: autoloading is disabled, not loading $i"
fi
# vim: set et ts=4:
